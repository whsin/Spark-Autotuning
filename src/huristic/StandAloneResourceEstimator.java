/**
 * 
 */
package huristic;
/**
*@author tonychao 
*创建时间 ：2018年4月20日 上午11:03:56
*功能  返回在standlone 模式下可以利用的资源
*/

import java.util.ArrayList;
import others.Global;

public class StandAloneResourceEstimator extends Estimator{

	//初始化时对estimater 进行赋值
	public StandAloneResourceEstimator(int executor_cores,int executor_memory,int cores_per_task){		
		super(executor_cores, executor_memory, cores_per_task);
	}
	
	//对estimator 拿到的数据进行估算
	public void conf(int executor_cores,int executor_memory,int cores_per_task){
		this.availableCores=this.avaliableMemory=this.avaliableExecutors=this.avalibaleParallelism=0;
		this.executor_cores=executor_cores; this.executor_memory=executor_memory; this.cores_per_task=cores_per_task;
		//计算每一个节点的可用executor数量
		int executor_on_this_node = -1;
		for (int i=0;i<memorys.size();i++){
			executor_on_this_node= (int) Math.min(Math.floor(memorys.get(i)/executor_memory),Math.floor(cores.get(i)/executor_cores));
			this.avaliableExecutors+=executor_on_this_node;	
		}
		this.avaliableMemory=this.avaliableExecutors*executor_memory;
		this.availableCores=this.avaliableExecutors*executor_cores;
		this.avalibaleParallelism=(int) (avaliableExecutors*Math.floor(executor_cores/cores_per_task));
	}
	
}
