package confs;
/**
*@author tonychao 
*枚举所有的参数的数据类型
*/
public class DataType {

    public static enum typeProperty  
    {  
        INT,  			//整形 ，内存 in MB
        DOUBLE, 
        BOOLEAN,
        CATEGORY_2,		//枚举类型，两类，比如TRUE-FALSE
        CATEGORY_3,		//压缩类型
        CATEGORY_4,		//只是备用
        CATEGORY_5,
        CATEGORY_6, 
        CATEGORY_7,
        CATEGORY_8,
        CATEGORY_9,
        CATEGORY_10,
        OTHER			//这个是留给以K计量，或者，是可能含有文字的属性
        
    };  
}
