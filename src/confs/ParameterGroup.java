/**
 * 
 */
package confs;
/**
*@author tonychao
*功能
*/

import java.util.ArrayList;
import others.DataType;

public class ParameterGroup {
	int time;
	ArrayList<SingleParameter> parameterList=null;
	
	public ParameterGroup(ArrayList<SingleParameter> parameterList,int time){
		this.time=time;
		this.parameterList=parameterList;
	}
	
	
	/*
	 * 将参数内容变成一个能够被预测模块使用的ArrayList
	 * */
	public ArrayList<Double> toDoubleList(){
		ArrayList<Double> ret= new ArrayList<Double>();
		for(SingleParameter tmp :parameterList){
			ret.add((double) tmp.value); 
		}
		return ret;
	}
	
	public ArrayList<String> toStringList() {
		ArrayList<String> ret= new ArrayList<String>();
		for(SingleParameter tmp :parameterList){
			ret.add( "\t--conf "+tmp.name+"="+tmp.toString()); 
		}
		return ret;
	}
	

}
