package fetcher;

import org.json.JSONException;
import org.json.JSONObject;

import others.Global;

public class TaskInfo {
	int stageID=-1;
	int stageAttemptID=-1;
	String taskType="";
	boolean taskSuccess = false;
	int taskID=-1;
	int index =-1;
	int taskAttempt =-1;
	long launchTime=-1;
	String executorID ="";
	String host,locality;
	boolean speculative=false;
	long gettingresultTime =-1;
	long finishTime=-1;
	boolean failed=false;
	boolean killed=false;
	
	//"Task Metrics" 的一部分
	long executorDeserializeTime=-1;
	long executorRunTime=-1;
	long JVMGCTime=-1;
	long resultSerializationTime=-1;
	//Task Metrics"-Input Metrics
	long bytesRead =0;
	
	
	
	/*
	 * source :"SparkListenerTaskEnd"
	 */
	public TaskInfo(JSONObject dataJson) throws JSONException {
		if (Global.DEBUG_FLAG) System.out.println("SparkListenerTaskEnd 没有考虑到多次attempt的情况");
		stageID=dataJson.getInt("Stage ID");
		stageAttemptID=dataJson.getInt("Stage Attempt ID");
		taskType=dataJson.getString("Task Type");
		String strSuccess = dataJson.getJSONObject("Task End Reason").getString("Reason");
		if(strSuccess.equals("Success")){
			taskSuccess=true;
		}
		else{
			taskSuccess=false;
		}
		//task info
		JSONObject jsonTaskInfo=dataJson.getJSONObject("Task Info");
		taskID=jsonTaskInfo.getInt("Task ID");
		index =jsonTaskInfo.getInt("Index");
		taskAttempt =jsonTaskInfo.getInt("Attempt");
		launchTime=jsonTaskInfo.getLong("Launch Time");
		executorID =jsonTaskInfo.getString("Executor ID");
		host=jsonTaskInfo.getString("Host");
		locality=jsonTaskInfo.getString("Locality");
		speculative=jsonTaskInfo.getBoolean("Speculative");
		gettingresultTime =jsonTaskInfo.getLong("Getting Result Time");
		finishTime=jsonTaskInfo.getLong("Finish Time");
		failed=jsonTaskInfo.getBoolean("Failed");
		killed=jsonTaskInfo.getBoolean("Killed");
		
		//任务如果失败就不会有task ID
		if(this.taskSuccess==false) return;
		//"Task Metrics" 的一部分
		/*
		 * "Task Metrics":{
			"Executor Deserialize Time":470,
			"Executor Deserialize CPU Time":71914835,
			"Executor Run Time":1960,
			"Executor CPU Time":1585702043,
			"Result Size":3951343,
			"JVM GC Time":148,
			"Result Serialization Time":77,
			"Memory Bytes Spilled":0,
			"Disk Bytes Spilled":0,
			"Shuffle Read Metrics":{
			"Remote Blocks Fetched":0,
			"Local Blocks Fetched":0,
			"Fetch Wait Time":0,
			"Remote Bytes Read":0,
			"Local Bytes Read":0,
			"Total Records Read":0
			},
			"Shuffle Write Metrics":{
			"Shuffle Bytes Written":0,
			"Shuffle Write Time":0,
			"Shuffle Records Written":0
			},
			"Input Metrics":{
			"Bytes Read":121214279,
			"Records Read":1851
			},
			"Output Metrics":{
			"Bytes Written":0,
			"Records Written":0
			},
			"Updated Blocks":[
			{
			"Block ID":"broadcast_0_piece0",
			"Status":{
			"Storage Level":{
			"Use Disk":false,
			"Use Memory":true,
			"Deserialized":false,
			"Replication":1
			},
			"Memory Size":20730,
			"Disk Size":0
			}
			},
			{
			"Block ID":"broadcast_0",
			"Status":{
			"Storage Level":{
			"Use Disk":false,
			"Use Memory":true,
			"Deserialized":true,
			"Replication":1
			},
			"Memory Size":291248,
			"Disk Size":0
			}
			}
			]
		 * 
		 * */
		//System.out.println(this.taskID);
		JSONObject jsonTaskMetrics= dataJson.getJSONObject("Task Metrics");
		
		this.executorDeserializeTime=jsonTaskMetrics.getLong("Executor Deserialize Time");
		this.executorRunTime=jsonTaskMetrics.getLong("Executor Run Time");
		this.JVMGCTime=jsonTaskMetrics.getLong("JVM GC Time");
		this.resultSerializationTime=jsonTaskMetrics.getLong("Result Serialization Time");
		
		
		//"Input Metrics"
		//Bytes Read
		JSONObject jsonObjectInputMetrics= jsonTaskMetrics.getJSONObject("Input Metrics");
		this.bytesRead=jsonObjectInputMetrics.getLong("Bytes Read");
	
	}
	

	
	public long getBytesRead() {
		return bytesRead;
	}



	public void setBytesRead(long bytesRead) {
		this.bytesRead = bytesRead;
	}



	public long getExecutorDeserializeTime() {
		return executorDeserializeTime;
	}

	public void setExecutorDeserializeTime(long executorDeserializeTime) {
		this.executorDeserializeTime = executorDeserializeTime;
	}

	public long getExecutorRunTime() {
		return executorRunTime;
	}

	public void setExecutorRunTime(long executorRunTime) {
		this.executorRunTime = executorRunTime;
	}

	public long getJVMGCTime() {
		return JVMGCTime;
	}

	public void setJVMGCTime(long jVMGCTime) {
		JVMGCTime = jVMGCTime;
	}

	public long getResultSerializationTime() {
		return resultSerializationTime;
	}

	public void setResultSerializationTime(long resultSerializationTime) {
		this.resultSerializationTime = resultSerializationTime;
	}

	//返回用时
	public long getUsedTime(){
		return this.finishTime-this.launchTime;
	}

	/*
	 * 下面都是getters 和setters
	 * 
	 * */

	public int getStageID() {
		return stageID;
	}





	public void setStageID(int stageID) {
		this.stageID = stageID;
	}





	public int getStageAttemptID() {
		return stageAttemptID;
	}





	public void setStageAttemptID(int stageAttemptID) {
		this.stageAttemptID = stageAttemptID;
	}





	public String getTaskType() {
		return taskType;
	}





	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}





	public boolean isTaskSuccess() {
		return taskSuccess;
	}





	public void setTaskSuccess(boolean taskSuccess) {
		this.taskSuccess = taskSuccess;
	}





	public int getTaskID() {
		return taskID;
	}





	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}





	public int getIndex() {
		return index;
	}





	public void setIndex(int index) {
		this.index = index;
	}





	public int getTaskAttempt() {
		return taskAttempt;
	}





	public void setTaskAttempt(int taskAttempt) {
		this.taskAttempt = taskAttempt;
	}





	public long getLaunchTime() {
		return launchTime;
	}





	public void setLaunchTime(long launchTime) {
		this.launchTime = launchTime;
	}





	public String getExecutorID() {
		return executorID;
	}





	public void setExecutorID(String executorID) {
		this.executorID = executorID;
	}





	public String getHost() {
		return host;
	}





	public void setHost(String host) {
		this.host = host;
	}





	public String getLocality() {
		return locality;
	}





	public void setLocality(String locality) {
		this.locality = locality;
	}





	public boolean isSpeculative() {
		return speculative;
	}





	public void setSpeculative(boolean speculative) {
		this.speculative = speculative;
	}





	public long getGettingresultTime() {
		return gettingresultTime;
	}





	public void setGettingresultTime(long gettingresultTime) {
		this.gettingresultTime = gettingresultTime;
	}





	public long getFinishTime() {
		return finishTime;
	}





	public void setFinishTime(long finishTime) {
		this.finishTime = finishTime;
	}





	public boolean isFailed() {
		return failed;
	}





	public void setFailed(boolean failed) {
		this.failed = failed;
	}





	public boolean isKilled() {
		return killed;
	}





	public void setKilled(boolean killed) {
		this.killed = killed;
	}



}
