package fetcher;
/**
*@author tonychao 
*功能 HTTP 协议接口，传入一个URL字符串，返回一个String
*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;  
import java.net.URLConnection;

import others.Global;  

public class HTTPWebFetcher {
    String urlStr;//外接口路径  
    URL url;
    String data;
    
	public String getData(String urlStr){		//URL
		this.urlStr=urlStr;
		try {
		    //链接URL  
			this.url = new URL(urlStr);
		    //返回结果集  
			 data = new String();  
		    //创建链接  
		   URLConnection conn = url.openConnection();  
		   //读取返回结果集  
		   BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
	       String line = null;  
	       while ((line = reader.readLine()) != null){  
	             data+=line;  
	       }     
	       reader.close();  
	       
	       if (Global.DEBUG_FLAG) System.out.println("[REST API GET]"+data);    
		} catch (IOException e) {
			System.err.println("[HTTPFetcher]我也不知道是哪里出错误了，但是就是没读到信息就对了");
			e.printStackTrace();
		} 
		return data;
	}
	
	public String getData(){
		return data;
	}
	
}
