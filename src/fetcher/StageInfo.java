package fetcher;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StageInfo {

	int ID=-1;
	int stageAttempID=-1;
	int numberOfTasks=-1;
	ArrayList<RDDInfo> RDDList=null;
	ArrayList<Integer> parentIDList=null;
	ArrayList<TaskInfo> taskList=null;
	long submissionTime=-1;
	long completionTime=-1;
	long inputBytes_APIONLY;
	public long getInputBytes_APIONLY() {
		return inputBytes_APIONLY;
	}


	public void setInputBytes_APIONLY(long inputBytes_APIONLY) {
		this.inputBytes_APIONLY = inputBytes_APIONLY;
	}




	boolean completed=false;


	String stageName;
	public String getStageName() {
		return stageName;
	}


	public void setStageName(String stageName) {
		this.stageName = stageName;
	}


	//Spark REST API only
	public StageInfo(){
		
	}
	

	/*
	 * 源Json："SparkListenerJobStart"-"Stage Infos"-一个对象
	 * */
	public StageInfo(JSONObject dataJson) throws JSONException {
		// TODO Auto-generated constructor stub
		
		ID=dataJson.getInt("Stage ID");
		stageAttempID=dataJson.getInt("Stage Attempt ID");
		stageName=dataJson.getString("Stage Name");
		numberOfTasks=dataJson.getInt("Number of Tasks");

		//RRD INfo
		RDDList =new ArrayList<RDDInfo>();
		JSONArray jsonStageList=dataJson.getJSONArray("RDD Info");
		for (int i =0;i<jsonStageList.length();i++){
			RDDInfo tmp =new RDDInfo( jsonStageList.getJSONObject(i));
			RDDList.add (tmp);
		}
		//json 对象，里面是Stage之间的依赖关系
		JSONArray JsonArrayParentList=dataJson.getJSONArray("Parent IDs");
		//如果为空
		this.parentIDList=new ArrayList<Integer>();
		for (int i=0;i<JsonArrayParentList.length();i++){
			this.parentIDList.add(JsonArrayParentList.getInt(i));
		}
		//处理task
		taskList= new ArrayList<TaskInfo>();
//		for (int i=0;i<this.numberOfTasks;i++){
//			taskList.add(null);
//		}
	}
	

	public long getRunTime(){
		return this.completionTime-this.submissionTime;
	}
	
	public boolean contains(TaskInfo tmptask) {
		if (tmptask.getStageID()==this.ID&&tmptask.stageAttemptID==this.stageAttempID)
			return true;
		return false;
	}
	
	public void addTask(TaskInfo task){
		taskList.add(task);
	}
	
	public ArrayList<TaskInfo> getTaskList() {
		return taskList;
	}



	public void setTaskList(ArrayList<TaskInfo> taskList) {
		this.taskList = taskList;
	}



	public boolean isCompleted() {
		return completed;
	}




	public void setCompleted(boolean completed) {
		this.completed = completed;
	}




	public long getSubmissionTime() {
		return submissionTime;
	}




	public void setSubmissionTime(long submissionTime) {
		this.submissionTime = submissionTime;
	}




	public long getCompletionTime() {
		return completionTime;
	}




	public void setCompletionTime(long completionTime) {
		this.completionTime = completionTime;
	}




	public int getID() {
		return ID;
	}




	public void setID(int iD) {
		ID = iD;
	}




	public int getStageAttempID() {
		return stageAttempID;
	}




	public void setStageAttempID(int stageAttempID) {
		this.stageAttempID = stageAttempID;
	}




	public int getNumberOfTasks() {
		return numberOfTasks;
	}




	public void setNumberOfTasks(int numberOfTasks) {
		this.numberOfTasks = numberOfTasks;
	}




	public ArrayList<RDDInfo> getRDDList() {
		return RDDList;
	}




	public void setRDDList(ArrayList<RDDInfo> rDDList) {
		RDDList = rDDList;
	}




	public ArrayList<Integer> getParentIDList() {
		return parentIDList;
	}




	public void setParentIDList(ArrayList<Integer> parentIDList) {
		this.parentIDList = parentIDList;
	}




	public String getName() {
		return stageName;
	}




	public void setName(String name) {
		this.stageName = name;
	}



}
